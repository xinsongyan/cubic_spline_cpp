#include <iostream>
#include <vector>

#include "swing_trajectory.h"


int main(int argc, char** argv)
{

    std::vector<double> T {0.0, 0.5, 1.0};
    std::vector<double> X {0.0, 0.05, 0.1};
    std::vector<double> Y {0.0, 0.05, 0.1};
    std::vector<double> Z {0.0, 0.1, 0.0};
    double t=0.356;

    std::cout << "Test CubicSpline!----------------------------------------------" << std::endl;
    CubicSpline1D cubic_spline(T,Z);
    std::cout << "t:" << t << " p:" << cubic_spline.pos(t) << std::endl;
    std::cout << "t:" << t << " v:" << cubic_spline.vel(t) << std::endl;
    std::cout << "t:" << t << " a:" << cubic_spline.acc(t) << std::endl;

    std::cout << "Test SwingTrajectory3D!----------------------------------------------" << std::endl;
    Eigen::Vector3d ini_pos(0.0, 0.0, 0.0);
    Eigen::Vector3d fin_pos(0.1, 0.1, 0.0);
    double swing_duration=1.0;
    double swing_height=0.1;
    SwingTrajectory3D swing_trajectory3D(ini_pos, fin_pos, swing_duration, swing_height);
    std::cout << "t:" << t << " pos: " << swing_trajectory3D.pos(t).transpose() << std::endl;
    std::cout << "t:" << t << " vel: " << swing_trajectory3D.vel(t).transpose() << std::endl;
    std::cout << "t:" << t << " acc: " << swing_trajectory3D.acc(t).transpose() << std::endl;

    std::cout << "t:" << t << " T.translation(): " << swing_trajectory3D.pose(t).translation().transpose() << std::endl;
    std::cout << "t:" << t << " T.rotation(): " << swing_trajectory3D.pose(t).rotation() << std::endl;
}
